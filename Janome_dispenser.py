# -*- coding: utf-8 -*-
"""
Created on Thu Apr  9 16:32:58 2020

@author: wender.daniel
wender.daniel@idea-ip.com
"""
import time
import binascii
import serial

class ser(object):
    
    def __init__(self, port, baud, rts):
        self.client = serial.Serial();
        self.client.port = port;
        self.client.timeout = None
        self.client.baudrate = baud;
        self.client.bytesize = 8;
        self.client.parity = 'N';
        self.client.stopbits = 1;
        self.client.timeout = 0;
        self.client.xonxoff = 0;
        self.client.rtscts = rts;
        try:
            self.client.open();
        except:
            print('Failed to open port ' + str(port));
            raise 'NotConnected'
        else:
            print(str(port) + ' is opened.');    
        
    def write(self, command):
        str1 = command + '\r';
        str1 = str1.encode();
        self.client.write(str1);

    def write_n(self, command):
        str1 = command + '\r';
        str1 = str1.encode();
        self.client.write(str1);
            
    def read(self):
        return [self.client.readlines()]

    def close(self):
        self.client.close();

class Janome(object):
    
    def __init__(self):   
        self.time_default = 0.05   

    def __del__(self):
        try:
            self.serial.close();
        except:
            return
        
    def connect(self, port):
        
        
        self.serial = ser(str(port), 9600, 1);
        
    def info(self):
        """
        Returns AASCII string with informations of Janome robot.
        """
        self.serial.write('$B072\r');
        time.sleep(self.time_default);
        return self.serial.read();
                                   
    def robot_status(self):
        """
        Returns the robot current status.
        Output is a ASCII string. See manual for informations. 
        """
        self.serial.write('$B173\r');
        time.sleep(self.time_default);
        return self.serial.read();

    def power_on(self):                                     # Robot must be in External Run mode
        """                                                 # Start channel has to be set to COM1
        Initialize the robot.
        This function should be executed after emergency button  pressed. 
        """
        self.serial.write('$R082\r');                       
        time.sleep(self.time_default);            
        return self.serial.read();           

    def home(self):
        """
        Move the robot to initial position (x = 0, y = 0, z = 0).
        """
        self.serial.write('$R284\r');
        time.sleep(self.time_default);
        return self.serial.read()
        
    def prog_number(self, num):
        """
        Change the program number.

        Parameters
        ____________________
        
        num: int
            program number
        """
        num = str(hex(num));            
        num = num[2:];
        num = num.zfill(4);                                 # Put the prog. number in a 4 caracters string
        string = '$R1' + num ;
        string = string.upper();                            # The letters of hexa must be upper case
        string = string  + self.sum_hex(string[1:]) + '\r';
        self.serial.write(string);
        time.sleep(self.time_default);
        return self.serial.read();
    
    def new_program(self, prog_num):
        """
        This function creates a new Janome program.

        Parameters
        ____________________
        
        prog_num: int
            New program number

        A string of error is returned if the program already exist.
        """
        prog_num = str(hex(prog_num));
        prog_num = prog_num[2:];
        prog_num = prog_num.zfill(4);
        
        string = '$S8' + prog_num;
        string = string.upper();
        string = string + self.sum_hex(string[1:]) + '\r';
        self.serial.write(string);
        time.sleep(self.time_default);
        return self.serial.read();
    
    def del_program(self, prog_num):
        """
        Delete the specified program.

        Parameters
        ____________________

        prog_num: int
            Program of the program to be deleted.
        """
        prog_num = str(hex(prog_num));
        prog_num = prog_num[2:];    
        prog_num = prog_num.zfill(4);
        
        string = '$S9' + prog_num;
        string = string.upper();
        string = string + self.sum_hex(string[1:]) + '\r';
        self.serial.write(string);
        time.sleep(self.time_default);
        return self.serial.read();
        
    def start(self):
        """
        Start the current program.
        """                                         
        string = '$R3';
        string = string + self.sum_hex(string[1:]) + '\r';
        self.serial.write(string);
        time.sleep(self.time_default);
        return self.serial.read();
    
    def run_point(self, num):
        """
        
        """                                         
        num = str(hex(num));            
        num = num[2:];
        num = num.zfill(4);                                 # Put the prog. number in a 4 caracters string
        string = '$R9' + num ;
        string = string.upper();                            # The letters of hexa must be upper case
        string = string  + self.sum_hex(string[1:]) + '\r';
        print(string)
        self.serial.write(string);
        time.sleep(self.time_default);
        return self.serial.read();


    def program_end(self):
        """
        Stop the execution of a program at the current point.
        """
        string = '$R7';
        string = string.upper();
        string = string + self.sum_hex(string[1:]) + '\r';
        self.serial.write(string);
        time.sleep(self.time_default);
        return self.serial.read();

    def move(self, x, y, z):
        """
        Move the robot to the desired position.
        Coordinates dimensions = mm.

        Parameters
        ____________________
        
        x: float
            x coordinate
        y: float
            y coordinate
        z: float
            z coordinate
        """
        aux = self.point_to_string(x, y, z);                # x ,y, z -> mm
        string = '$M1' + aux;
        string = string.upper();
        string = string + self.sum_hex(string[1:]) + '\r';
        self.serial.write(string);
        time.sleep(self.time_default);
        return self.serial.read();

    def arm_position(self):                                 
        """
        Returns the robot current position.
        """
        self.serial.write('$N07E\r');
        time.sleep(self.time_default);
        pos =  self.serial.read();
        if(len(pos[0][0]) != 30):
            self.serial.write('$N07E\r');
            time.sleep(self.time_default);
            pos =  self.serial.read();
        pos = pos[0][0];
        pos = str(pos);
        pos = pos[5:-4];
        x = int(('0x' + pos[0:6]), 16);                     # Converting return position to mm
        y = int(('0x' + pos[6:12]), 16);
        z = int(('0x' + pos[12:18]), 16);
        x = x*0.0005;
        y = y*0.0005;
        z = z*0.0005;
        return x, y, z;

    def num_points(self, prog_num):                         
        """
        Return the number of points of a program
        """
        prog_num = str(hex(prog_num));
        prog_num = prog_num[2:];
        prog_num = prog_num.zfill(4);
        string = '$S0' + prog_num;
        string = string.upper();
        string = string + self.sum_hex(string[1:]) + '\r';
        self.serial.write(string);
        time.sleep(self.time_default);
        n = self.serial.read();
        n = str(n[0][0]);
        n = n[9:17];
        n = int(('0x' + n), 16);
        return n;
    
    def add_point(self, prog_num, point_type, x, y, z, speed = 0, t = 0):  
        """
        Add a point to the end of the program.

        Parameters
        ____________________

        prog_num: int
            Program number
        point_type: int
            Type of dispensing point
            1      -> Dispense point 
            2      -> Start line dispense
            3      -> End Line Dispense
            4      -> PTP point (just move the robot to a position)
            5      -> Line passsing point
            More information see Janome JR 2200N communication manual.
        x: float
            x point coordinate [mm]
        y: float
            y point coordinate [mm]
        z: float
            z point coordinate [mm]
        speed: float, optional
            dispensing line speed (point_type = 2,3,5) [mm/s]
        t: float, optional 
            time of dispensing point (point_type = 1) [s]
        """
        prog_num = str(hex(prog_num));
        prog_num = prog_num[2:];                       
        prog_num = prog_num.zfill(4);                   
                                                       
        if(point_type == 1):                           
            point_type = '697F0121';                   
            t = str(hex(int(t/0.01)));                 
            t = t[2:];                                 
            t = t.zfill(4);
            t = t.upper();
            
            speed = int(speed/0.1);
            speed = str(hex(speed));
            speed = speed[2:];
            speed = speed.zfill(8);
            speed = speed.upper();
            rest = '0';                    
            rest2 = '0';
            rest = rest.zfill(36);
            rest2 = rest2.zfill(16);
            aux = self.point_to_string(x, y, z);
            point = point_type + aux + speed + rest + t + rest2; 
            string = '$SB' + prog_num  + point;
            string = string.upper();
            string = string + self.sum_hex(string[1:]) + '\r';
        
        if(point_type == 2):
            point_type = '67350122';        # Point type code
            speed = int(speed/0.1);
            speed = str(hex(speed));
            speed = speed[2:];
            speed = speed.zfill(8);
            speed = speed.upper();

            rest = '0';                    
            rest2 = '0';
            rest = rest.zfill(56);
            aux = self.point_to_string(x, y, z);
            point = point_type + aux + speed + rest; 
            string = '$SB' + prog_num + point;
            string = string.upper()
            string = string + self.sum_hex(string[1:]) + '\r';
            
        if(point_type == 3):
            point_type = 'F0300123';
            speed = int(speed/0.1);
            speed = str(hex(speed));
            speed = speed[2:];
            speed = speed.zfill(8);
            speed = speed.upper();

            rest = '0';                    
            rest2 = '0';
            rest = rest.zfill(56);
            aux = self.point_to_string(x, y, z);
            point = point_type + aux + speed + rest; 
            string = '$SB' + prog_num + point;
            string = string.upper()
            string = string + self.sum_hex(string[1:]) + '\r';
            
        if(point_type == 4):                           
            point_type = '00000021';
            rest = '0';
            rest = rest.zfill(63);
            aux = self.point_to_string(x, y, z);
            point = point_type + aux + rest;
            string = '$SB' + prog_num  + point;
            string = string.upper();
            string = string + self.sum_hex(string[1:]) + '\r';
            
        if(point_type == 5):
            point_type = 'A7930124';
            speed = int(speed/0.1);
            speed = str(hex(speed));
            speed = speed[2:];
            speed = speed.zfill(8);
            speed = speed.upper();

            rest = '0';                    
            rest2 = '0';
            rest = rest.zfill(56);
            aux = self.point_to_string(x, y, z);
            point = point_type + aux + speed + rest; 
            string = '$SB' + prog_num + point;
            string = string.upper()
            string = string + self.sum_hex(string[1:]) + '\r';
            
        self.serial.write(string);
        time.sleep(self.time_default);
        return self.serial.read();

    def del_point_data(self, prog_num, point_num):
        """
        Delete the data of the referenced point in the designated program.

        Parameters
        ____________________

        prog_num: int
            Program number
        point_num: int
            point number
        """
        prog_num = str(hex(prog_num));
        prog_num = prog_num[2:];
        prog_num = prog_num.zfill(4);
        point_num = str(hex(point_num));
        point_num = point_num[2:];
        point_num = point_num.zfill(8);
    
        string = '$SD' + prog_num + point_num;
        string = string.upper() + self.sum_hex(string[1:]) + '\r';
        self.serial.write(string);
        time.sleep(self.time_default);
        return self.serial.read();

    def set_position(self, prog_num, point_num, x, y, z):
        """
        This function set the position of a program point.

        Parameters
        ____________________

        prog_num: int
            Program number
        point_num: int
            Point number
        x: float
            x point coordinate
        y: float
            y point coordinate
        z: float
            z point coordinate
        """
        prog_num = str(hex(prog_num));
        prog_num = prog_num[2:];
        prog_num = prog_num.zfill(4);
        point_num = str(hex(point_num));
        point_num = point_num[2:];
        point_num = point_num.zfill(8);

        aux = self.point_to_string(x, y, z);

        string = '$S1' + prog_num + point_num + aux;
        string = string.upper();
        string = string + self.sum_hex(string[1:]) + '\r';
        self.serial.write(string);
        time.sleep(self.time_default);
        return self.serial.read();

    def point_data(self, prog_num, point_num, point_type, x, y, z, speed = 0, t = 0):
        """
        Sets the parameters of a point in a program.

        Parameters
        ____________________

        prog_num: int
            Program number
        point_num: int
            Point number
        point_type: int
            point type
        x: float
            x point coordinate
        y: float
            y point coordinate
        z: float
            z point coordinate
        speed: float, optional
            dispensing line speed [mm/s]
        t: float, optional
            time of dispensing point [s]
        """
        prog_num = str(hex(prog_num));
        prog_num = prog_num[2:];
        prog_num = prog_num.zfill(4);
        point_num = str(hex(point_num));
        point_num = point_num[2:];
        point_num = point_num.zfill(8);
        
        if(point_type == 1):
            point_type = '697F0121';
            t = str(hex(int(t/0.01)));
            t = t[2:];
            t = t.zfill(4);
            t = t.upper();
            
            speed = int(speed/0.1);
            speed = str(hex(speed));
            speed = speed[2:];
            speed = speed.zfill(8);
            speed = speed.upper();
            rest = '0';                    
            rest2 = '0';
            rest = rest.zfill(36);
            rest2 = rest2.zfill(16);
            aux = self.point_to_string(x, y, z);
            point = point_type + aux + speed + rest + t + rest2; 
            string = '$S2' + prog_num + point_num + point;
            string = string.upper();
            string = string + self.sum_hex(string[1:]) + '\r';
        
        if(point_type == 2):
            point_type = '67350122';
            speed = int(speed/0.1);
            speed = str(hex(speed));
            speed = speed[2:];
            speed = speed.zfill(8);
            speed = speed.upper();
            
            rest = '0';
            rest = rest.zfill(56);
            aux = self.point_to_string(x, y, z);
            point = point_type + aux + speed + rest; 
            string = '$S2' + prog_num + point_num + point;
            string = string.upper();
            string = string + self.sum_hex(string[1:]) + '\r';
            
        if(point_type == 3):
            point_type = 'F0300123';
            speed = int(speed/0.1);
            speed = str(hex(speed));
            speed = speed[2:];
            speed = speed.zfill(8);
            speed = speed.upper();
            
            rest = '0';
            rest = rest.zfill(56);
            aux = self.point_to_string(x, y, z);
            point = point_type + aux + speed + rest; 
            string = '$S2' + prog_num + point_num + point;
            string = string.upper();
            string = string + self.sum_hex(string[1:]) + '\r';
        
        self.serial.write(string);
        time.sleep(self.time_default);
        return self.serial.read();

    def point_info(self, prog_num, point_num):
        """
        Returns the data of a point in an string with ASCII caracters

        Parameters
        ____________________

        prog_num: int
            Program number
        point_num: int
            Point number
        """
        prog_num = str(hex(prog_num));
        prog_num = prog_num[2:];
        prog_num = prog_num.zfill(4);
        point_num = str(hex(point_num));
        point_num = point_num[2:];
        point_num = point_num.zfill(8);

        string = '$SA' + prog_num + point_num;
        string = string.upper();
        string = string + self.sum_hex(string[1:]) + '\r';
        self.serial.write(string);
        time.sleep(self.time_default);
        return self.serial.read();

    def set_velocity(self, prog_num, x_speed, y_speed, z_speed, z_move_height = 50):
        """
        Set the axis velocities for a designated program.

        Parameters
        ____________________

        prog_num: int
            Program number
        point_num: int
            Point number
        x_speed: float
            velocity of x axis in percentage of maximum velocity
        y_speed: float
            velocity of y axis in percentage of maximum velocity
        z_speed: float
            velocity of z axis in percentage of maximum velocity
        z_move_height: float, optional
            distance that the arm move up in each dispense point
        """
        prog_num = str(hex(prog_num));
        prog_num = prog_num[2:];
        prog_num = prog_num.zfill(4);                           
        x_speed = int(x_speed/0.01);                            
        x_speed = str(hex(x_speed));
        x_speed = x_speed[2:];
        x_speed = x_speed.zfill(8);
        y_speed = int(y_speed/0.01);
        y_speed = str(hex(y_speed));
        y_speed = y_speed[2:];
        y_speed = y_speed.zfill(8);
        z_speed = int(z_speed/0.01);
        z_speed = str(hex(z_speed));
        z_speed = z_speed[2:];
        z_speed = z_speed.zfill(8);
        
        max_acel = '00002710';                                  # Maximum acceleration
        arc_motion = '0'.zfill(8);
        z_move_height = int(z_move_height/0.001);
        z_move_height = str(hex(z_move_height))[2:];
        z_move_height = z_move_height.zfill(8);
        ptp_condition = x_speed + max_acel + y_speed + max_acel + z_speed + max_acel + \
        z_speed + max_acel + arc_motion + z_move_height + z_move_height + z_move_height \
        + '0'.zfill(48);
        
        string = '$SH' + prog_num + '00030000' + ptp_condition;
        string = string.upper();
        string = string + self.sum_hex(string[1:]) + '\r';
        self.serial.write(string);
        time.sleep(self.time_default);
        return self.serial.read();

    def prog_data_request(self, prog_num):
        """
        Returns the program paramets in a ASCII string.

        Parameters
        ____________________

        prog_num: int
            Program number
        """
        prog_num = str(hex(prog_num));
        prog_num = prog_num[2:];
        prog_num = prog_num.zfill(4);

        string = '$SI' + prog_num + '000b0000';
        string = string.upper();
        string = string + self.sum_hex(string[1:]) + '\r';
        self.serial.write(string);
        time.sleep(self.time_default);
        return self.serial.read();
    
    def prog_name(self, prog_num, name):
        """
        Set the program name.

        Parameters
        ____________________

        prog_num: int
            program number
        name: string
            program name
        """                        
        prog_num = str(hex(prog_num));
        prog_num = prog_num[2:];
        prog_num = prog_num.zfill(4);
        name = str(name);
        name = binascii.hexlify((name).encode()).decode();      # Convert name caacters for ASCII
        name = name.ljust(240,'0');                             # Fill string with zeros in the right
        string = '$SH' + prog_num + '00000000' + name;
        string = string.upper();
        string = string + self.sum_hex(string[1:]);
        self.serial.write(string);
        time.sleep(self.time_default);
        return self.serial.read();

    def axis_limit(self, prog_num, x_max, y_max, z_max):        
        """
        Set limits for the three axis.
        
        Parameters
        ____________________

        prog_num: int
            program number
        x_max:
            maximun x coodinate 
        y_max:
            maximun y coodinate
        z_max:
            maximun z coodinate
        """
        prog_num = str(hex(prog_num));                          
        prog_num = prog_num[2:];
        prog_num = prog_num.zfill(4);
        x_max = int(x_max/0.001);
        x_max = str(hex(x_max));
        x_max = x_max[2:];
        x_max = x_max.zfill(8);
        y_max = int(y_max/0.001);
        y_max = str(hex(y_max));
        y_max = y_max[2:];
        y_max = y_max.zfill(8);
        z_max = int(z_max/0.001);
        z_max = str(hex(z_max));
        z_max = z_max[2:];
        z_max = z_max.zfill(8);
                                    # Low limits set to zero, but can be changed here
        data = x_max + '0'.zfill(8) + y_max + '0'.zfill(8) + z_max + '0'.zfill(16);
        string = '$SH' + prog_num + '00050000' + data;
        string = string.upper();
        string = string + self.sum_hex(string[1:]) + '\r';
        self.serial.write(string);
        time.sleep(self.time_default);
        return self.serial.read(); 

    def cycle_mode(self, prog_num, cycle):
        """
        Define the cycle mode of a program.


        Parameters
        ____________________

        prog_num: int
            program number
        cycle: int
            cycle = 0 : run 1 cycle
            cycle = 1 : continuous playback
        """
        prog_num = str(hex(prog_num));                          
        prog_num = prog_num[2:];                                
        prog_num = prog_num.zfill(4);
        cycle = str(cycle).zfill(8);

        string = '$SH' + prog_num + '00020000' + cycle;
        string = string.upper();
        string = string + self.sum_hex(string[1:]) + '\r';
        self.serial.write(string);
        time.sleep(self.time_default);
        return self.serial.read();

    def save(self):                                             
        """
        Save all data to non valatile memory.
        """
        string = '$T084'
        self.serial.write(string);
        time.sleep(self.time_default);
        return self.serial.read();

    def start_jog(self, axis, direction, speed):
        string = '$M4';
        
        axis = str(hex(axis))[2:];
        axis = axis.zfill(2);

        direction = str(hex(direction))[2:]
        direction = direction.zfill(2)

        speed = str(hex(speed))[2:];
        axis = axis.zfill(2);
        
        toll_data = '';
        toll_data = toll_data.zfill(28); 
        string = string + '00' + axis + direction + speed + toll_data;
        string = string + self.sum_hex(string[1:]) + '\r';
        self.serial.write(string);
        time.sleep(self.time_default);
        return self.serial.read();

    def jog_request(self):
        string = '$M5';

        speed = '01';
        
        string = string + speed;
        string = string + self.sum_hex(string[1:]) + '\r';
        self.serial.write(string);
        time.sleep(self.time_default);
        return self.serial.read();


    def stop_jog(self):
        string = '$M683';
        self.serial.write(string);
        time.sleep(self.time_default);
        return self.serial.read();

    def sum_hex(self, s):
        """
        Return the commands required SUM code.

        Parameters
        ____________________

        s: string
            all command string before BCC code 
        """                                       
        result = int('0x0', 16);
        for i in range(len(s)):
            a = binascii.hexlify((s[i]).encode()).decode();     #Return a hexa string of the ASCII simbol
            a = '0x' + a;
            result = result + int(a, 16);
        result = hex(result);
        result = str(result)
        result = result[-2] + result[-1];
        result = result.upper();
        return result;

    def point_to_string(self, x, y, z):     
        """
        Convert coodinates to string.

        Parameters
        ____________________

        x:
            x coodinate 
        y:
            y coodinate
        z:
            z coodinate
        """             
        x = int(x/0.0005);
        y = int(y/0.0005);
        z = int(z/0.0005);
        x = x|1;
        x = str(hex(x))[2:];
        y = str(hex(y))[2:];
        z = str(hex(z))[2:];
        x = x.upper();
        y = y.upper();
        z = z.upper();
        x = x.zfill(6);
        y = y.zfill(6);
        z = z.zfill(6);
        R = '0';
        R = R.zfill(6);
        return x + y + z + R;
    
    def IO_readout(self, type_value):
        type_value = str(hex(type_value));
        type_value = type_value[2:];
        type_value = type_value.zfill(4);
        
        string = '$K0' + type_value;
        string = string.upper();
        string = string + self.sum_hex(string[1:]) #+ '\r';
        self.serial.write(string);
        time.sleep(self.time_default);
        c = self.serial.read();
        c = str(c[0][0]);
        c = c[9:15];
        c1 = c[0:2];
        c2 = c[2:4];
        c3 = c[4:6];
        c1 = bin(int('0x' + c1, 16));
        c1 = c1[2:];
        c1 = c1.zfill(8);                   
        c2 = bin(int('0x' + c2, 16));
        c2 = c2[2:];
        c2 = c2.zfill(8);
        c3 = bin(int('0x' + c3, 16));
        c3 = c3[2:];
        c3 = c3.zfill(2);
                 
        return c1, c2, c3;
        
    def output_set(self, type_value, num):
        type_value = str(hex(type_value));
        type_value = type_value[2:];
        type_value = type_value.zfill(4);
        
        num = str(hex(num));
        num = num[2:];
        num = num.zfill(8);
        string = '$K2' + type_value + num;
        string = string.upper();
        string = string + self.sum_hex(string[1:]) + '\r';
        self.serial.write(string);
        time.sleep(self.time_default);
        return self.serial.read();
    
    def output_reset(self, type_value, num):
        type_value = str(hex(type_value));
        type_value = type_value[2:];
        type_value = type_value.zfill(4);
        
        num = str(hex(num));
        num = num[2:];
        num = num.zfill(8);
        string = '$K3' + type_value + num;
        string = string.upper();
        string = string + self.sum_hex(string[1:]) + '\r';
        self.serial.write(string);
        time.sleep(self.time_default);
        return self.serial.read();
    
    def output_delay_set(self, type_value, num, t):             # t in ms
        type_value = str(hex(type_value));
        type_value = type_value[2:];
        type_value = type_value.zfill(4);
        
        num = str(hex(num));
        num = num[2:];
        num = num.zfill(8);
        
        t = str(hex(t));
        t = t[2:];
        t = t.zfill(8);
        
        string = '$K4' + type_value + num + t;
        string = string.upper();
        string = string + self.sum_hex(string[1:]) + '\r';
        self.serial.write(string);
        time.sleep(self.time_default);
        return self.serial.read();
    
    def pulse(self, type_value, num, width):
        type_value = str(hex(type_value));
        type_value = type_value[2:];
        type_value = type_value.zfill(4);
        
        num = str(hex(num));
        num = num[2:];
        num = num.zfill(8);
        
        width = str(hex(width));                                # width in ms
        width = width[2:];
        width = width.zfill(8);
        
        string = '$K6' + type_value + num + width;
        string = string.upper();
        string = string + self.sum_hex(string[1:]) + '\r';
        self.serial.write(string);
        time.sleep(self.time_default);
        return self.serial.read();
        
    def delay_pulse_set(self, type_value, num, t, width):
        type_value = str(hex(type_value));
        type_value = type_value[2:];
        type_value = type_value.zfill(4);
        
        num = str(hex(num));
        num = num[2:];
        num = num.zfill(8);
        
        width = str(hex(width));                                # width in ms
        width = width[2:];
        width = width.zfill(8);
        
        t = str(hex(t));
        t = t[2:];
        t = t.zfill(8);
        
        string = '$K8' + type_value + num + t + width;
        string = string.upper();
        string = string + self.sum_hex(string[1:]) + '\r';
        self.serial.write(string);
        time.sleep(self.time_default);
        return self.serial.read();
    