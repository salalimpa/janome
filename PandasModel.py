from PyQt5 import QtCore
from PyQt5.QtCore import pyqtSlot, Qt, QTimer, QLocale
import pandas as pd


class PandasModel(QtCore.QAbstractTableModel):
    def __init__(self, df=pd.DataFrame(), parent=None):
        QtCore.QAbstractTableModel.__init__(self, parent=parent)
        self._df = df

    def headerData(self, section, orientation, role=QtCore.Qt.DisplayRole):
        if role != QtCore.Qt.DisplayRole:
            return QtCore.QVariant()

        if orientation == QtCore.Qt.Horizontal:
            try:
                return self._df.columns.tolist()[section]
            except (IndexError,):
                return QtCore.QVariant()
        elif orientation == QtCore.Qt.Vertical:
            try:
                # return self.df.index.tolist()
                return self._df.index.tolist()[section]
            except (IndexError,):
                return QtCore.QVariant()

    def data(self, index, role):
        if not index.isValid(): return QtCore.QVariant()
        row = index.row()
        column = index.column()

        if row > len(self._df): return QtCore.QVariant()
        if column > len(self._df.iloc[index.row()]): return QtCore.QVariant()

        if role == Qt.EditRole or role == Qt.DisplayRole:
            return QtCore.QVariant(str(self._df.iloc[index.row(), index.column()]))

        return QtCore.QVariant()

    def setData(self, index, value, role=Qt.EditRole):
        if index.isValid():
            if role == Qt.EditRole:
                row = index.row()
                col = index.column()

                if row > len(self._df) or col > len(self._df.iloc[row]):
                    return False
                else:
                    self._df.iloc[row, col] = value
                    return True
        return False

    # def data(self, index, role):
    #     if role != QtCore.Qt.DisplayRole:
    #         return QtCore.QVariant()
    #
    #     if not index.isValid():
    #         return QtCore.QVariant()
    #
    #     return QtCore.QVariant(str(self._df.iloc[index.row(), index.column()]))
    #
    # def setData(self, index, value, role):
    #     row = self._df.index[index.row()]
    #     col = self._df.columns[index.column()]
    #     if hasattr(value, 'toPyObject'):
    #         # PyQt4 gets a QVariant
    #         value = value.toPyObject()
    #     else:
    #         # PySide gets an unicode
    #         dtype = self._df[col].dtype
    #         if dtype != object:
    #             value = None if value == '' else dtype.type(value)
    #     if role == Qt.EditRole:
    #         print('cheguei aqui')
    #     self._df.iloc[index.row(), index.column()] = value
    #     # self._df.set_value(row, col, value)
    #     return True

    def rowCount(self, parent=QtCore.QModelIndex()):
        return len(self._df.index)

    def columnCount(self, parent=QtCore.QModelIndex()):
        return len(self._df.columns)

    def sort(self, column, order):
        colname = self._df.columns.tolist()[column]
        self.layoutAboutToBeChanged.emit()
        self._df.sort_values(colname, ascending=order == QtCore.Qt.AscendingOrder, inplace=True)
        self._df.reset_index(inplace=True, drop=True)
        self.layoutChanged.emit()

    def flags(self, index):
        return QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
