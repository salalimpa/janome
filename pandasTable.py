import sys

from PyQt5.QtCore import QLocale
from PyQt5.QtCore import QThread
import pandas as pd
from PyQt5.QtWidgets import *
from PyQt5 import QtCore, QtWidgets, QtGui
from PandasModel import PandasModel
import worker
import Janome_dispenser as Jan
import time
import serial.tools.list_ports
import traceback



class Delegate(QtWidgets.QItemDelegate):
    def __init__(self, owner, choices):
        super().__init__(owner)
        self.items = choices

    def createEditor(self, parent, option, index):
        self.editor = QtWidgets.QComboBox(parent)
        self.editor.currentIndexChanged.connect(self.commit_editor)
        self.editor.addItems(self.items)
        return self.editor

    def paint(self, painter, option, index):
        value = index.data(QtCore.Qt.DisplayRole)
        style = QtWidgets.QApplication.style()
        opt = QtWidgets.QStyleOptionComboBox()
        opt.text = str(value)
        opt.rect = option.rect
        style.drawComplexControl(QtWidgets.QStyle.CC_ComboBox, opt, painter)
        QtWidgets.QItemDelegate.paint(self, painter, option, index)

    def commit_editor(self):  ####test
        editor = self.sender()
        self.commitData.emit(editor)

    def setEditorData(self, editor, index):
        value = index.data(QtCore.Qt.DisplayRole)
        num = self.items.index(value)
        editor.setCurrentIndex(num)

    def setModelData(self, editor, model, index):
        value = editor.currentText()
        model.setData(index, value, QtCore.Qt.EditRole)

    def updateEditorGeometry(self, editor, option, index):
        editor.setGeometry(option.rect)


class TableWindow(QWidget):

    def __init__(self):
        super().__init__()
        self.setGeometry(60, 60, 1500, 600)
        QLocale.setDefault(QLocale(QLocale.English, QLocale.UnitedStates))
        self.setWindowTitle('Pick and Place Janome')
        app_icon = QtGui.QIcon()
        app_icon.addFile('gui\\settings.ico', QtCore.QSize(32,32))
        self.setWindowIcon(app_icon)
        self.initUI()
        self.prog_num = 19
        self.connect()

        self.validators = {'1': ['Pick', 'Place', 'Pass', 'Z Pass','Z Place', 'Line Start','Line End','Point Dispense', 'Execute Program']}
        
        self.obj = worker.Worker(Janome=self.Janome, prog_num = self.prog_num)
        self.thread = QThread()
        self.obj.moveToThread(self.thread)
        self.obj.finished.connect(self.thread.quit)
        self.thread.started.connect(self.obj.Jog)
        self.obj.signal.connect(self.write_point)

    def connect(self):
        ports = serial.tools.list_ports.comports()
        for port, desc, hwid in sorted(ports):
            if "Porta de comunicação" in desc:
                self.Janome = Jan.Janome()
                self.Janome.connect(port = port)
                time.sleep(1)
                self.Janome.power_on()
                self.Janome.serial.read()
                print("{}: {} [{}]".format(port, desc, hwid))
                self.Janome.del_program(self.prog_num);
                time.sleep(0.15);
                self.Janome.new_program(self.prog_num);
                self.Janome.set_velocity(prog_num = self.prog_num, x_speed = 10, y_speed = 10, z_speed = 2, z_move_height = 3);
                self.Janome.axis_limit(prog_num = self.prog_num, x_max = 150, y_max = 200, z_max = 45);
                self.Janome.cycle_mode(prog_num = self.prog_num, cycle = 0);
                self.Janome.prog_name(prog_num = self.prog_num, name = 'PC Control');
                self.Janome.prog_number(self.prog_num)
                
                
    def del_program(self):
        self.Janome.del_program(self.prog_num);
        time.sleep(0.15);
        self.Janome.new_program(self.prog_num);
        self.Janome.set_velocity(prog_num = self.prog_num, x_speed = 10, y_speed = 10, z_speed = 2, z_move_height = 3);
        self.Janome.axis_limit(prog_num = self.prog_num, x_max = 150, y_max = 200, z_max = 45);
        self.Janome.cycle_mode(prog_num = self.prog_num, cycle = 0);
        self.Janome.prog_name(prog_num = self.prog_num, name = 'PC Control');
        self.Janome.prog_number(self.prog_num)

    def construct_table(self):
        self.model = PandasModel(self.df)
        self.table_data.setModel(self.model)
        for keys in self.validators.keys():
            self.table_data.setItemDelegateForColumn(int(keys), Delegate(self, self.validators[keys]))
            for row in range(len(self.df.index)):
                self.table_data.openPersistentEditor(self.model.index(row, int(keys)))

    def load_data(self):
        
        try:
            path = "Processos\\"
            fileName, _ = QFileDialog.getOpenFileName(None, "Carregar arquivo", path,
                                                      "Points Data (*.csv)")
#            names = ['Name', 'Type', 'x', 'y', 'z', 'Time', 'Speed']
            self.df = pd.read_csv(fileName)#, names=names)
            self.construct_table()
            
            f = str(fileName).split('/')[-1]
            p = str(fileName).split('/')[-2]
            mp = str(fileName).split('/')[-3]
            self.welcom.setText(f'Arquivo carregado: {mp}/{p}/{f}')
            self.welcom.setStyleSheet('color: blue; font-size: 18px; ')

            
        except:
            pass

    def wait_moving(self):
        timeout = 2
        b = self.Janome.robot_status()
        start = time.time()
        tout = 0
        while b == [[]] and tout <timeout:
            b = self.Janome.robot_status()
            time.sleep(0.05)
            tout = time.time() - start


    def print_data(self):
        time_default = 0.15
        n = 0
        for i in range(len(self.df.index)):
            for key in self.df.keys():
                if key == 'Type':
                    if self.df.loc[i,key] == 'Point Dispense' or self.df.loc[i,key] == 'Line Start' or self.df.loc[i,key] == 'Line End':
                        if n ==0:
                            self.del_program()
                            n=1
                elif key == 'Name':
                    pass
                elif type(self.df.loc[i,key]) == str:   
                    try:
                        exec(self.df.loc[i,key])
                    except Exception as e:
                        print(traceback.format_exc())
                        return # pode travar algo? 

         
        self.aux_df = self.df.copy()
        indexes = self.table_data.selectionModel().selectedRows()
        for index in sorted(indexes):
            for key in self.df.keys():
                aux = self.df.loc[index.row(), key]
                if type(aux) == str:
                    if not '=' in aux and key !='Name' and key !='Type':
                        try:
                            self.aux_df.loc[index.row(), key] = eval(aux)

                        except Exception as e:
                            print(f'Erro no ponto {index.row()}:\n{e}')


            name = str(self.aux_df['Name'].iloc[index.row()])
            sp = self.aux_df['Speed'].iloc[index.row()]
            tp = str(self.aux_df['Type'].iloc[index.row()])
            x = self.aux_df['x'].iloc[index.row()]
            y = self.aux_df['y'].iloc[index.row()]
            z = self.aux_df['z'].iloc[index.row()]
            dx = self.aux_df['Delta x'].iloc[index.row()]
            dy = self.aux_df['Delta y'].iloc[index.row()]
            dz = self.aux_df['Delta z'].iloc[index.row()]
            t1 = self.aux_df.loc[index.row(),'Time']
            
                    
            if tp == 'Line Start': 

                self.Janome.add_point(prog_num = self.prog_num, point_type = 2, x = x, y = y, z = z, speed = sp)

            elif tp == 'Line End':    
                self.Janome.add_point(prog_num = self.prog_num, point_type = 3, x = x, y = y, z = z, speed = sp)
            
            elif tp == 'Point Dispense':    
                self.Janome.add_point(prog_num = self.prog_num, point_type = 1, x = x, y = y, z = z, t = t1, speed=sp)    

            elif tp == 'Execute Program':
                self.Janome.save()
                self.Janome.start()
                

            elif tp == "Z Place" or tp == 'Z Pass':
                self.Janome.set_velocity(prog_num = self.prog_num, x_speed = sp, 
                                     y_speed = sp, z_speed = sp/2, z_move_height = 0.0)
                time.sleep(time_default)

                try:
                    x_now,y_now,z_now = self.Janome.arm_position()
                except:
                    time.sleep(time_default)
                    x_now,y_now,z_now = self.Janome.arm_position()

                # if abs(x-x_now)>2.0 or abs(y-y_now)>2.0 or (z-z_now)>-0.2:
                #     print('wrong choice of point type, this will destroy something')
                #     print(f'\n x={x_now}, delta x ={abs(x-x_now)} \t y={y_now}, delta y = {abs(y-y_now)} \t z={z_now}, delta z = {z-z_now}', )
                # else:
                a = self.Janome.move(x =x, y = y, z = z )
                self.wait_moving()

                if tp == 'Z Place':
                    self.Janome.output_reset(3, 11)
                    time.sleep(t1)

                                           
            else:   
                self.Janome.set_velocity(prog_num = self.prog_num, x_speed = sp, 
                                                 y_speed = sp, z_speed = sp/2, z_move_height = 8.0)                     
                try:
                    x = float(x)
                    y = float(y)
                    z = float(z)
                    dx = float(dx)
                    dy = float(dy)
                    dz = float(dz)
                    a = self.Janome.move(x =x, y = y, z = z )
                    self.wait_moving()

                    sleeping = float(t1)
                    if tp == 'Pass':
                        time.sleep(sleeping)
                    
                    if tp == 'Pick':
                        self.Janome.output_set(3, 11)
                        time.sleep(sleeping)

                        self.Janome.set_velocity(prog_num = self.prog_num, x_speed = sp, 
                                                 y_speed = sp, z_speed = sp/2, 
                                                 z_move_height = 0.0)
                        if dx ==0.0 and dy ==0.0:
                            a = self.Janome.move(x =x, y = y, z = z-dz )
                            self.wait_moving()
                        else:
                            a = self.Janome.move(x =x, y = y, z = z-dz )
                            
                            self.wait_moving()
                            a = self.Janome.move(x = x+dx, y = y, z = z-dz )
                            self.wait_moving()
                            a = self.Janome.move(x = x-dx, y = y, z = z-dz )
                            self.wait_moving()
                            a = self.Janome.move(x = x, y = y, z = z-dz )
                            self.wait_moving()
                            a = self.Janome.move(x = x, y = y+dy, z = z-dz )
                            self.wait_moving()
                            a = self.Janome.move(x = x, y = y-dy, z = z-dz )
                            self.wait_moving()
                            a = self.Janome.move(x = x, y = y, z = z-dz )
                            self.wait_moving()

                        self.Janome.set_velocity(prog_num = self.prog_num, x_speed = sp, 
                                                 y_speed = sp, z_speed = sp/2, 
                                                 z_move_height = 8.0)
                    
                    
                    elif tp == 'Place' or tp == 'Z Place':
                        self.Janome.output_reset(3, 11)
                        time.sleep(sleeping)
                    
                except Exception as e:
                    print(e)
    
    def toggle_vacuum(self):
        try:
            _, c, _,  = self.Janome.IO_readout(3)
            
            if c[-3] == '1':
                self.Janome.output_reset(3, 11)
            else:
                self.Janome.output_set(3, 11)
        except:
            pass
            
    
    def save_data(self):
        try:
            fileName, _ = QFileDialog.getSaveFileName(self, 'Dialog Title', '', '*.csv')
            self.df.to_csv(fileName, index=False)
        except:
            pass
        
    def add_row(self):
        try:
            d = {'Name': '', 'Type': 'Pass', 'x': 0.0, 'y': 0.0, 'z': 0.0,
                 'Time': 1.0, 'Speed': 10.0, 'Delta x': 0.05, 'Delta y': 0.05, 'Delta z': 0.15}

            self.df = self.df.append(d, ignore_index=True)
            self.construct_table()
        except Exception as e:
            print(e)
    def remove_row(self):

        indexes = self.table_data.selectionModel().selectedRows()
        for index in sorted(indexes):
            # df = df[df.index != x]
            self.df = self.df[self.df.index != index.row()]

        self.construct_table()

    def get_point(self):
        self.thread.start()



    def write_point(self, num):
        indexes = self.table_data.selectionModel().selectedRows()
        for index in sorted(indexes):
            print(index.row())
            x = float(num[0])
            y = float(num[1])
            z = float(num[2])
            
            d = [' ', 'Pass', x,  y, z, 1.0, 10.0, 0.05,0.05,0.150]
            try:
                self.df.iloc[index.row()] = d
                self.construct_table()
            except:
                self.df = self.df.append(d, ignore_index=True)
                self.construct_table()


    def create_table(self):
        index = [0]
        columns = ['Name', 'Type', 'x', 'y', 'z', 'Time', 'Speed', 'Delta x', 'Delta y', 'Delta z']
        self.df = pd.DataFrame(index=index, columns=columns)
        self.df.iloc[0] = ['', 'Pass', 0.0,0.0,0.0, 1.0, 10.0, 0.05, 0.05, 0.150]

        self.construct_table()

    def reset(self):
        self.Janome.power_on()
        self.Janome.serial.read()
    
    def home(self):
        self.Janome.set_velocity(prog_num = self.prog_num, x_speed = 5, 
                                 y_speed = 5, z_speed = 5, z_move_height = 8.0)          
        self.Janome.move(0.0,0.0,0.0)
        self.wait_moving()
        

    def initUI(self):

        self.welcom = QLabel('Controle Janome')

        self.btn_get_point = QPushButton('Get point')
        self.btn_get_point.clicked.connect(self.get_point)

        self.btn_print_data = QPushButton('Run Points')
        self.btn_print_data.setStyleSheet("background-color: green; color: red;")
        self.btn_print_data.clicked.connect(self.print_data)

        self.btn_save_data = QPushButton('save data')
        self.btn_save_data.clicked.connect(self.save_data)

        self.btn_show_table = QPushButton('Load file')
        self.btn_show_table.clicked.connect(self.load_data)

        self.btn_create_table = QPushButton('Create Empty table')
        self.btn_create_table.clicked.connect(self.create_table)

        self.btn_add_row = QPushButton('Add Row')
        self.btn_add_row.clicked.connect(self.add_row)

        self.btn_remove_row = QPushButton('remove Row')
        self.btn_remove_row.clicked.connect(self.remove_row)

        self.btn_toggle_vacuum = QPushButton('Toggle Vacuum')
        self.btn_toggle_vacuum.clicked.connect(self.toggle_vacuum)
        
        self.btn_home = QPushButton('Home')
        self.btn_home.clicked.connect(self.home)
        
        self.btn_reset = QPushButton('Reset')
        self.btn_reset.setStyleSheet("background-color: red; color: blue;")
        self.btn_reset.clicked.connect(self.reset)

        self.table_data = QTableView()

        # self.table_result = QTableView()

        hbox1 = QHBoxLayout()
        hbox1.addWidget(self.welcom)

        grid = QGridLayout()
        grid.addWidget(self.btn_show_table,2,0)
        grid.addWidget(self.btn_create_table,2,1)
        grid.addWidget(self.btn_add_row,3,0)
        grid.addWidget(self.btn_remove_row,3,1)
        grid.addWidget(self.btn_save_data,4,0)
        grid.addWidget(self.btn_get_point,4,1)
        grid.addWidget(self.btn_print_data,5,1)
        grid.addWidget(self.btn_toggle_vacuum,5,0)
        grid.addWidget(self.btn_reset,6,0)
        grid.addWidget(self.btn_home,6,1)

        vbox3 = QVBoxLayout()
        vbox3.addWidget(self.table_data)
        # vbox3.addWidget(self.table_result)

        hbox2 = QHBoxLayout()
        hbox2.addLayout(grid)
        hbox2.addLayout(vbox3)

        vbox1 = QVBoxLayout()
        vbox1.addLayout(hbox1)
        vbox1.addLayout(hbox2)

        self.setLayout(vbox1)
        self.show()


if __name__ == '__main__':
    # Back up the reference to the exceptionhook
    import sys
    import serial

    #with serial.Serial('COM3', 9600, timeout=0) as ser
        #pass
    sys._excepthook = sys.excepthook


    def my_exception_hook(exctype, value, traceback):
        # Print the error and traceback
        print(exctype, value, traceback)
        # Call the normal Exception hook after
        sys._excepthook(exctype, value, traceback)
        #sys.exit(1)


    # Set the exception hook to our wrapping function
    sys.excepthook = my_exception_hook
    app = QApplication(sys.argv)
    ex = TableWindow()
    try:

        sys.exit(app.exec_())
    except:
        print("Exiting")
        ex.Janome.serial.close()

