ELEMENTO:
Mascara  : 1.50 X 2.20 mm^2
Frame    : 1.30 X 2.00 mm^2
Etalon 1 : 1.20 X 1.80 mm^2
Phase Sh : 1.20 X 1.80 mm^2
Etalon 2 : 1.10 X 1.70 mm^2

---------------------------------------
Deltas dos Elementos em relação com a Mascara:
Frame    : de_xx = 0.12 , de_yy = 0.12
Etalon 1 : de_xx = 0.17 , de_yy = 0.22
Phase Sh : de_xx = 0.17 , de_yy = 0.22
Etalon 2 : de_xx = 0.22 , de_yy = 0.27

------------------------------
Colas:   tm_ho -- tm_ve   
Et1 :     0.5  --  4.0 
Et2 :     0.5  --  2.0
PSh :     0.5  --  3.0
-----------------------------





