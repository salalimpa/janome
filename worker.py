# worker.py
from PyQt5.QtCore import QThread, QObject, pyqtSignal, pyqtSlot
import time
import keyboard
import numpy as np



class Worker(QObject):
    finished = pyqtSignal()
    signal = pyqtSignal(object)

    def __init__(self, Janome, prog_num,  parent=None):
        QThread.__init__(self, parent)
        self.Janome = Janome
        self.prog_num = prog_num
#        self.df = df
        
#    @pyqtSlot()
#    def Run(self):
#        for index, row in self.df.iterrows():
#            print(row['Execute?'], row['x'])
#        
#        self.finished.emit()

    @pyqtSlot()
    def Jog(self):
        self.Janome.del_program(self.prog_num)
        self.Janome.new_program(self.prog_num)
        self.Janome.axis_limit(prog_num = self.prog_num, x_max = 150, y_max = 200, z_max = 45);
        
        self.Janome.prog_number(self.prog_num)
        
        step = 0.02
        key_before = ''
        
        while(1):
            key = keyboard.read_key()
            print(key)
                   
            self.Janome.set_velocity(prog_num = self.prog_num, x_speed = 10, y_speed = 10, z_speed = 1, z_move_height = 5)
            
            if(key == "left"):
                self.Janome.start_jog(axis = 1, direction = 1, speed = 0)
                while(keyboard.is_pressed('left')):
                    self.Janome.jog_request()
                self.Janome.stop_jog()
            elif(key == "right"):
                self.Janome.start_jog(axis = 1, direction = 0, speed = 0)
                while(keyboard.is_pressed('right')):
                    self.Janome.jog_request()
                self.Janome.stop_jog()

            elif(key == "up"):
                self.Janome.start_jog(axis = 0, direction = 0, speed = 0)
                while(keyboard.is_pressed('up')):
                    self.Janome.jog_request()
                self.Janome.stop_jog()
            elif(key == "down"):
                self.Janome.start_jog(axis = 0, direction = 1, speed = 0)
                while(keyboard.is_pressed('down')):
                    self.Janome.jog_request()
                self.Janome.stop_jog()    
            elif(key == "page down"):
                self.Janome.start_jog(axis = 2, direction = 0, speed = 0)
                while(keyboard.is_pressed('page down')):
                    self.Janome.jog_request()
                self.Janome.stop_jog()
            elif(key == "page up"):
                self.Janome.start_jog(axis = 2, direction = 1, speed = 0)
                while(keyboard.is_pressed('page up')):
                    self.Janome.jog_request()
                self.Janome.stop_jog()
            elif (key == "ctrl"):
                self.Janome.set_velocity(prog_num = self.prog_num, x_speed = 10, y_speed = 10, z_speed = 1, z_move_height = 0)
                
                while(keyboard.is_pressed("ctrl")):
                    x,y,z = self.Janome.arm_position()
                    key = keyboard.read_key()
                    if(key == "page up"):
                        z = z - step
                        
                    if(key == "page down"):
                        z = z + step

                    if(key == "down"):
                        y = y + step
                        
                    if(key == "up"):
                        y = y - step                         
                    
                    if(key == "left"):
                        x = x+step
                    
                    if(key == "right"):
                        x= x-step

                    print(x,y,z)
                    self.Janome.move(x,y,z)   
                    
            

            elif(key == "esc"):
                 break
            
           
              
            key_before = key
        num = self.Janome.arm_position()
        self.signal.emit(num)

        self.finished.emit()